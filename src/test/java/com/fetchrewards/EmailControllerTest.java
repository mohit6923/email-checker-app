package com.fetchrewards;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fetchrewards.service.EmailService;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.util.ResourceUtils;

import java.util.List;

import static com.fetchrewards.EmailController.GREETINGS;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmailControllerTest {

    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();
    private List<String> emails;

    @Test
    public void calculateUniqueEmailAddresses_shouldReturn_noOfUniqueEmailAddresses() throws Exception {
        String EXPECTED_NO_OF_EMAILS = "2";
        emails = objectMapper.readValue(
                ResourceUtils.getFile("classpath:emails.json"), new TypeReference<List<String>>() {
                });

        ResultActions resultActions = mockMvc
                .perform(MockMvcRequestBuilders.post("/api/v1/fetchrewards/uniqueEmails")
                        .content(objectMapper.writeValueAsString(emails)).contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(EXPECTED_NO_OF_EMAILS)));
    }

    @Test
    public void getHello_shouldReturn_greetings() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/fetchrewards/hello").accept(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(equalTo(GREETINGS)));
    }

}
