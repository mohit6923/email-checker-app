package com.fetchrewards.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class EmailServiceImplTest {

    private EmailService emailService = new EmailServiceImpl();

    @Test
    public void test_numUniqueEmails_shouldReturnNumberOfUniqueEmailAddresses() {
        int EXPECTED_NO_OF_UNIQUE_EMAILS = 2;
        List<String> emails = Arrays.asList("test.email@gmail.com", "test.email+spam@gmail.com",
                "testemail@gmail.com", "testemail+22@gmail.com", "teste.ma.il@gmail.com", "testemail",
                "testemail@yahoo.com");

        int uniqueEmails = emailService.numUniqueEmails(emails);

        assertEquals(EXPECTED_NO_OF_UNIQUE_EMAILS, uniqueEmails);
    }

    @Test
    public void test_numUniqueEmails_shouldReturnZero_whenNoEmailsArePresentInRequest() {
        int EXPECTED_NO_OF_UNIQUE_EMAILS = 0;

        int uniqueEmails = emailService.numUniqueEmails(null);
        assertEquals(EXPECTED_NO_OF_UNIQUE_EMAILS, uniqueEmails);

        uniqueEmails = emailService.numUniqueEmails(Collections.emptyList());
        assertEquals(EXPECTED_NO_OF_UNIQUE_EMAILS, uniqueEmails);
    }
}
