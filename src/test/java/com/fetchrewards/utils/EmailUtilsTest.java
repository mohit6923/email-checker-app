package com.fetchrewards.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(MockitoJUnitRunner.class)
public class EmailUtilsTest {

    @Test
    public void test_createEffectiveEmailAddress_shouldReturnVa() {
        String EFFECTIVE_EMAIL = "testemail@gmail.com";
        List<String> emails = Arrays.asList("test.email@gmail.com", "test.email+spam@gmail.com",
                "testemail@gmail.com", "testemail+22@gmail.com", "teste.ma.il@gmail.com");

        List<String> effectiveEmails = emails.stream()
                .map(EmailUtils::createEffectiveEmailAddress)
                .collect(Collectors.toList());

        assertNotNull(effectiveEmails);
        assertEquals(emails.size(), effectiveEmails.size());

        effectiveEmails.forEach(effectiveEmail -> assertEquals(EFFECTIVE_EMAIL, effectiveEmail));
    }
}
