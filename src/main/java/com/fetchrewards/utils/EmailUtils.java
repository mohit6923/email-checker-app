package com.fetchrewards.utils;

public class EmailUtils {
    public static String createEffectiveEmailAddress(String email) {
        int domainStartIndex = email.indexOf('@');
        int endIndex = domainStartIndex;

        if (email.contains("+") && email.indexOf('+') < domainStartIndex)
            endIndex = email.indexOf('+');

        return email.substring(0, endIndex)
                .replaceAll("\\.", "")
                + email.substring(domainStartIndex);
    }
}
