package com.fetchrewards;

import com.fetchrewards.service.EmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/fetchrewards")
public class EmailController {

    static final String GREETINGS = "Hello from Fetch Rewards!";
    private Logger logger = LoggerFactory.getLogger(EmailController.class);

    @Autowired
    private EmailService emailService;

    @RequestMapping(value = "/uniqueEmails", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Integer> calculateUniqueEmailAddresses(@RequestBody List<String> emails) {
        int uniqueEmails = emailService.numUniqueEmails(emails);

        logger.info("Found {} unique email(s)", uniqueEmails);
        return ResponseEntity.status(HttpStatus.OK).body(uniqueEmails);
    }

    @RequestMapping(value = "/hello", method = RequestMethod.GET)
    public String index() {
        return GREETINGS;
    }
}
