package com.fetchrewards.service;

import com.fetchrewards.utils.EmailUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmailServiceImpl implements EmailService {
    @Override
    public int numUniqueEmails(List<String> emails) {
        if (!Optional.ofNullable(emails).isPresent())
            return 0;

        return emails.stream()
                .filter(x -> x.contains("@"))
                .map(EmailUtils::createEffectiveEmailAddress)
                .collect(Collectors.toSet())
                .size();
    }
}
