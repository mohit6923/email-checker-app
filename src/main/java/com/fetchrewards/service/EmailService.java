package com.fetchrewards.service;

import java.util.List;

public interface EmailService {
    int numUniqueEmails(List<String> emails);
}
